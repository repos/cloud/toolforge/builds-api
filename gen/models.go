// Package gen provides primitives to interact with the openapi HTTP API.
//
// Code generated by github.com/deepmap/oapi-codegen/v2 version v2.1.0 DO NOT EDIT.
package gen

const (
	KeyScopes = "key.Scopes"
)

// Defines values for BuildStatus.
const (
	BUILDCANCELLED BuildStatus = "BUILD_CANCELLED"
	BUILDFAILURE   BuildStatus = "BUILD_FAILURE"
	BUILDRUNNING   BuildStatus = "BUILD_RUNNING"
	BUILDSUCCESS   BuildStatus = "BUILD_SUCCESS"
	BUILDTIMEOUT   BuildStatus = "BUILD_TIMEOUT"
	BUILDUNKNOWN   BuildStatus = "BUILD_UNKNOWN"
)

// Defines values for HealthResponseStatus.
const (
	ERROR HealthResponseStatus = "ERROR"
	OK    HealthResponseStatus = "OK"
)

// Build defines model for Build.
type Build struct {
	BuildId          *string          `json:"build_id,omitempty"`
	DestinationImage *string          `json:"destination_image,omitempty"`
	EndTime          *string          `json:"end_time,omitempty"`
	Message          *string          `json:"message,omitempty"`
	Parameters       *BuildParameters `json:"parameters,omitempty"`
	StartTime        *string          `json:"start_time,omitempty"`
	Status           *BuildStatus     `json:"status,omitempty"`
}

// BuildCondition defines model for BuildCondition.
type BuildCondition struct {
	Message *string      `json:"message,omitempty"`
	Status  *BuildStatus `json:"status,omitempty"`
}

// BuildLog defines model for BuildLog.
type BuildLog struct {
	Line *string `json:"line,omitempty"`
}

// BuildParameters defines model for BuildParameters.
type BuildParameters struct {
	// Envvars Environment variables and values to be set at build time
	Envvars *map[string]string `json:"envvars,omitempty"`

	// ImageName The name of the image to be built
	ImageName *string `json:"image_name,omitempty"`

	// Ref Source code reference to build (ex. a git branch name)
	Ref *string `json:"ref,omitempty"`

	// SourceUrl URL to the public git repository that contains the source code
	// to build
	SourceUrl string `json:"source_url"`
}

// BuildStatus defines model for BuildStatus.
type BuildStatus string

// CancelResponse defines model for CancelResponse.
type CancelResponse struct {
	// Id Id of the cancelled build
	Id *string `json:"id,omitempty"`

	// Messages Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
	// with information on how to proceed to update it.
	Messages *ResponseMessages `json:"messages,omitempty"`
}

// CleanResponse defines model for CleanResponse.
type CleanResponse struct {
	// Messages Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
	// with information on how to proceed to update it.
	Messages *ResponseMessages `json:"messages,omitempty"`
}

// DeleteResponse defines model for DeleteResponse.
type DeleteResponse struct {
	// Id Id of the deleted build
	Id *string `json:"id,omitempty"`

	// Messages Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
	// with information on how to proceed to update it.
	Messages *ResponseMessages `json:"messages,omitempty"`
}

// GetResponse defines model for GetResponse.
type GetResponse struct {
	Build *Build `json:"build,omitempty"`

	// Messages Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
	// with information on how to proceed to update it.
	Messages *ResponseMessages `json:"messages,omitempty"`
}

// HealthResponse defines model for HealthResponse.
type HealthResponse struct {
	// Messages Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
	// with information on how to proceed to update it.
	Messages *ResponseMessages     `json:"messages,omitempty"`
	Status   *HealthResponseStatus `json:"status,omitempty"`
}

// HealthResponseStatus defines model for HealthResponse.Status.
type HealthResponseStatus string

// LatestResponse defines model for LatestResponse.
type LatestResponse struct {
	Build *Build `json:"build,omitempty"`

	// Messages Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
	// with information on how to proceed to update it.
	Messages *ResponseMessages `json:"messages,omitempty"`
}

// ListResponse defines model for ListResponse.
type ListResponse struct {
	Builds *[]Build `json:"builds,omitempty"`

	// Messages Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
	// with information on how to proceed to update it.
	Messages *ResponseMessages `json:"messages,omitempty"`
}

// NewBuild defines model for NewBuild.
type NewBuild struct {
	Name       *string          `json:"name,omitempty"`
	Parameters *BuildParameters `json:"parameters,omitempty"`
}

// Quota defines model for Quota.
type Quota struct {
	Categories *[]QuotaCategory `json:"categories,omitempty"`
}

// QuotaCategory defines model for QuotaCategory.
type QuotaCategory struct {
	Items *[]QuotaCategoryItem `json:"items,omitempty"`
	Name  *string              `json:"name,omitempty"`
}

// QuotaCategoryItem defines model for QuotaCategoryItem.
type QuotaCategoryItem struct {
	Available *string `json:"available,omitempty"`
	Capacity  *string `json:"capacity,omitempty"`
	Limit     *string `json:"limit,omitempty"`
	Name      *string `json:"name,omitempty"`
	Used      *string `json:"used,omitempty"`
}

// QuotaResponse defines model for QuotaResponse.
type QuotaResponse struct {
	// Messages Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
	// with information on how to proceed to update it.
	Messages *ResponseMessages `json:"messages,omitempty"`
	Quota    *Quota            `json:"quota,omitempty"`
}

// ResponseMessages Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
// with information on how to proceed to update it.
type ResponseMessages struct {
	Error   *[]string `json:"error,omitempty"`
	Info    *[]string `json:"info,omitempty"`
	Warning *[]string `json:"warning,omitempty"`
}

// StartResponse defines model for StartResponse.
type StartResponse struct {
	// Messages Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
	// with information on how to proceed to update it.
	Messages *ResponseMessages `json:"messages,omitempty"`
	NewBuild *NewBuild         `json:"new_build,omitempty"`
}

// BadParameters Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
// with information on how to proceed to update it.
type BadParameters = ResponseMessages

// Conflict Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
// with information on how to proceed to update it.
type Conflict = ResponseMessages

// InternalError Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
// with information on how to proceed to update it.
type InternalError = ResponseMessages

// NotFound Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
// with information on how to proceed to update it.
type NotFound = ResponseMessages

// Unauthorized Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated
// with information on how to proceed to update it.
type Unauthorized = ResponseMessages

// LogsParams defines parameters for Logs.
type LogsParams struct {
	// Follow Follow the logs
	Follow *bool `form:"follow,omitempty" json:"follow,omitempty"`
}

// StartJSONRequestBody defines body for Start for application/json ContentType.
type StartJSONRequestBody = BuildParameters
